package com.atlassian.confluence.plugins.macros.advanced.xhtml;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.basic.CssSizeValue;
import com.atlassian.renderer.v2.macro.basic.validator.BorderStyleValidator;
import com.atlassian.renderer.v2.macro.basic.validator.ColorStyleValidator;
import com.atlassian.renderer.v2.macro.basic.validator.CssSizeValidator;
import com.atlassian.renderer.v2.macro.basic.validator.MacroParameterValidationException;
import com.atlassian.renderer.v2.macro.basic.validator.ValidatedMacroParameters;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * Abstract class for panel macros. Your implementation will need to provide css classes for panel container, panel
 * header and panel body. These CSS classes will define the default look and feel. Users can override these default
 * styles through styles specified in macro parameters.
 *
 * @deprecated Since 2.1.2. Have your macro delegate to the {@link MacroPanel} if possible.
 */
public abstract class AbstractPanelMacro implements Macro
{
    protected abstract String getPanelCSSClass();

    protected abstract String getPanelHeaderCSSClass();

    protected abstract String getPanelContentCSSClass();

    protected String getBodyContent(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        return body;
    }

    protected String getTitle(Map<String, String> parameters, String body)
    {
        return parameters.get("title");
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext conversionContext) throws MacroExecutionException
    {
        StringBuilder buffer = new StringBuilder(body.length() + 100);

        String title = getTitle(parameters, body);
        body = getBodyContent(parameters, body, conversionContext);

        ValidatedMacroParameters validatedParameters = new ValidatedMacroParameters(parameters);
        validatedParameters.setValidator("borderStyle", BorderStyleValidator.getInstance());
        validatedParameters.setValidator("borderColor", ColorStyleValidator.getInstance());
        validatedParameters.setValidator("bgColor", ColorStyleValidator.getInstance());
        validatedParameters.setValidator("titleBGColor", ColorStyleValidator.getInstance());
        validatedParameters.setValidator("borderWidth", CssSizeValidator.getInstance());

        String borderStyle;
        String borderColor;
        String backgroundColor;
        String titleBackgroundColor;
        String borderWidthString;
        try
        {
            borderStyle = validatedParameters.getValue("borderStyle");
            borderColor = validatedParameters.getValue("borderColor");
            backgroundColor = validatedParameters.getValue("bgColor");
            titleBackgroundColor = validatedParameters.getValue("titleBGColor");
            borderWidthString = validatedParameters.getValue("borderWidth");
        } catch (MacroParameterValidationException e)
        {
            throw new MacroExecutionException(e);
        }

        int borderWidth = 1;
        if (borderWidthString != null)
        {
            CssSizeValue cssBorderWidth = new CssSizeValue(borderWidthString);
            borderWidth = cssBorderWidth.value();
        }

        Map explicitStyles = prepareExplicitStyles(borderWidth, borderStyle, borderColor, backgroundColor);

        if (StringUtils.isBlank(titleBackgroundColor) && StringUtils.isNotBlank(backgroundColor))
            titleBackgroundColor = backgroundColor;

        buffer.append("<div class=\"").append(getPanelCSSClass()).append("\"");

        if (explicitStyles.size() > 0)
            handleExplicitStyles(buffer, explicitStyles);

        buffer.append(">");

        if (StringUtils.isNotBlank((title)))
            writeHeader(conversionContext.getPageContext(), buffer, title, borderStyle, borderColor, borderWidth, titleBackgroundColor);
        if (StringUtils.isNotBlank(body))
            writeContent(buffer, parameters, body, backgroundColor);

        buffer.append("</div>");

        return buffer.toString();
    }

    private void handleExplicitStyles(StringBuilder buffer, Map explicitStyles)
    {
        buffer.append(" style=\"");

        for (Iterator iterator = explicitStyles.keySet().iterator(); iterator.hasNext();)
        {
            String styleAttribute = (String) iterator.next();
            String styleValue = (String) explicitStyles.get(styleAttribute);
            buffer.append(styleAttribute).append(": ").append(styleValue).append(";");
        }

        buffer.append("\"");
    }

    private Map prepareExplicitStyles(int borderWidth, String borderStyle, String borderColor, String backgroundColor)
    {
        Map explicitStyles = new TreeMap();

        explicitStyles.put("border-width", borderWidth + "px");
        if (borderWidth > 0)
        {
            if (StringUtils.isNotBlank(borderStyle))
                explicitStyles.put("border-style", borderStyle);
            if (StringUtils.isNotBlank(borderColor))
                explicitStyles.put("border-color", borderColor);
        }
        else
        {
            // hack to make borderless panels look right (the bottom margins of block level elements like <p> and <ul>
            // do NOT pad out the bottom of the panel body unless there is a border
            if (borderWidth == 0)
                explicitStyles.put("border-bottom", "1px solid white");
        }

        if (StringUtils.isNotBlank(backgroundColor))
            explicitStyles.put("background-color", backgroundColor);
        return explicitStyles;
    }

    protected void writeHeader(RenderContext renderContext, StringBuilder buffer, String title, String borderStyle,
        String borderColor, int borderWidth, String titleBackgroundColor)
    {
        buffer.append("<div class=\"").append(getPanelHeaderCSSClass()).append("\"").append(
            renderContext.isRenderingForWysiwyg() ? " wysiwyg=\"ignore\" " : "");

        buffer.append(" style=\"");

        buffer.append("border-bottom-width: ").append(borderWidth).append("px;");
        if (borderWidth > 0)
        {
            if (StringUtils.isNotBlank(borderStyle))
                buffer.append("border-bottom-style: ").append(borderStyle).append(";");
            if (StringUtils.isNotBlank(borderColor))
                buffer.append("border-bottom-color: ").append(borderColor).append(";");
        }
        if (StringUtils.isNotBlank(titleBackgroundColor))
            buffer.append("background-color: ").append(titleBackgroundColor).append(";");

        buffer.append("\"");
        buffer.append("><b>");
        buffer.append(StringEscapeUtils.escapeHtml(title));
        buffer.append("</b></div>");
    }

    protected void writeContent(StringBuilder buffer, Map parameters, String content, String backgroundColor)
    {
        buffer.append("<div class=\"").append(getPanelContentCSSClass()).append("\"");
        if (StringUtils.isNotBlank(backgroundColor))
            buffer.append(" style=\"background-color: ").append(backgroundColor).append(";\"");
        buffer.append(">\n");
        buffer.append(content.trim());
        buffer.append("\n</div>");
    }
}
