package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContextPathHolder;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.macro.ContentFilteringMacro;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.macro.params.ParameterException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchWithToken;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import com.atlassian.confluence.util.i18n.I18NBean;
import com.atlassian.confluence.util.i18n.I18NBeanFactory;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.atlassian.confluence.macro.Macro.BodyType.NONE;
import static com.atlassian.confluence.macro.Macro.OutputType.BLOCK;
import static com.atlassian.confluence.renderer.radeox.macros.MacroUtils.defaultVelocityContext;
import static com.atlassian.renderer.v2.RenderMode.NO_RENDER;
import static com.google.common.collect.Iterables.isEmpty;
import static org.apache.commons.lang.StringUtils.isNotBlank;

public class RecentlyChangedContentMacro extends ContentFilteringMacro implements Macro
{
    private static final Logger log = LoggerFactory.getLogger(RecentlyChangedContentMacro.class);
    private static final String MACRO_NAME = "recently-updated";

    private UserAccessor userAccessor;
    private FormatSettingsManager formatSettingsManager;
    private LocaleManager localeManager;
    private I18NBeanFactory i18NBeanFactory;
    private ContentTypesDisplayMapper contentTypesDisplayMapper;
    private ContextPathHolder contextPathHolder;

    public RecentlyChangedContentMacro()
    {
        contentTypeParam.addParameterAlias("types");
        contentTypeParam.setDefaultValue(getDefaultTypeParamValue());
        spaceKeyParam.setDefaultValue(DEFAULT_SPACE_KEY);
        maxResultsParam.setDefaultValue(DEFAULT_MAX_RESULTS);
    }

    @Override
    protected String execute(MacroExecutionContext macroExecutionContext) throws MacroException
    {
        try
        {
            return execute(macroExecutionContext.getParams(), macroExecutionContext.getBody(), new DefaultConversionContext(macroExecutionContext.getPageContext()));
        }
        catch (MacroExecutionException ex)
        {
            throw new MacroException(ex.getCause() != null ? ex.getCause() : ex);
        }
    }

    @Override
    public String execute(final Map<String, String> parameters, final String body, final ConversionContext conversionContext)
            throws MacroExecutionException
    {
        /**
         * Using setter injection to acquire this bean will produce one that does not include plugin bundles/keys.
         * Resolve by acquiring one during execution time.
         */
        final User user = AuthenticatedUserThreadLocal.get();
        final I18NBean i18n = i18NBeanFactory.getI18NBean(localeManager.getLocale(user));

        // retrieve parameter values
        final String authorParamValue = authorParam.getParameterValue(parameters);
        final String labelParamValue = labelParam.getParameterValue(parameters);
        final String contentTypeParamValue = contentTypeParam.getParameterValue(parameters);
        final String spaceKeyParamValue = getSpaceKeyParamValue(parameters, conversionContext);
        final Theme theme = getTheme(parameters);
        final int maxResults = getMaxResults(new MacroExecutionContext(parameters, body, conversionContext.getPageContext()), i18n);
        final boolean hideHeading = getHideHeading(parameters);
        final String paramFilter = parameters.get("filter");
        final String paramWidth = parameters.get("width");

        final RecentChangesSearchBuilder searchBuilder = new RecentChangesSearchBuilder();
        searchBuilder.withLabels(labelParamValue).withAuthors(authorParamValue).withContentTypes(contentTypeParamValue).withSpaceKeys(spaceKeyParamValue).withPageSize(maxResults);

        final Map<String, Object> macroRenderContext = getMacroVelocityContext();
        if (isNotBlank(paramWidth))
        {
            macroRenderContext.put("width", paramWidth);
        }

        final ISearch changesSearch = searchBuilder.buildSearch();

        final SearchResults searchResults;
        try
        {
            searchResults = searchManager.search(changesSearch);
        }
        catch (InvalidSearchException e)
        {
            throw new MacroExecutionException(e);
        }

        final boolean noResults = isEmpty(searchResults);

        if (!hideHeading || noResults)
        {
            macroRenderContext.put("title", i18n.getText("recently.updated"));
            macroRenderContext.put("titleHeadingId", conversionContext.getPageContext().getElementIdCreator().generateId("recently-updated-macro"));
        }
        if (noResults)
        {
            return renderEmptyTemplate(macroRenderContext);
        }

        final ConfluenceUserPreferences pref = userAccessor.getConfluenceUserPreferences(AuthenticatedUserThreadLocal.get());
        final DateFormatter dateFormatter = pref.getDateFormatter(formatSettingsManager, localeManager);
        final UpdateItemFactory updateItemFactory = new DefaultUpdateItemFactory(dateFormatter, i18n, contentTypesDisplayMapper);

        macroRenderContext.put("changesUrl", searchBuilder.buildSearchUrl(theme, contextPathHolder.getContextPath()));

        if (isNotBlank(paramFilter))
        {
            if ("sidebar".equals("theme"))
            {
                throw new MacroExecutionException("Filter control is only supported in concise and social at the moment.");
            }
            if (isNotBlank(parameters.get("type")) || isNotBlank(parameters.get("types")))
            {
                throw new MacroExecutionException("Filter control is only supported when no type/types parameter is specified.");
            }

            macroRenderContext.put("filter", paramFilter);
        }

        /**
         * Used for content type filter drop-down
         */
        macroRenderContext.put("contentTypes", getContentTypes());

        if (!searchResults.isLastPage())
        {
            final SearchWithToken nextPageSearch = searchResults.getNextPageSearch();

            searchBuilder.withStartIndex(nextPageSearch.getStartOffset()).withPageSize(nextPageSearch.getLimit()).withSearchToken(nextPageSearch.getSearchToken());

            macroRenderContext.put("nextPageUrl", searchBuilder.buildSearchUrl(theme, contextPathHolder.getContextPath()));
        }

        if (RenderContextOutputType.HTML_EXPORT.equals(conversionContext.getOutputType()))
        {
            macroRenderContext.put("performingHtmlExport", true);
        }

        final List<UpdateItem> updateItems = new LinkedList<UpdateItem>();
        for (final SearchResult searchResult : searchResults)
        {
            final UpdateItem updateItem = updateItemFactory.get(searchResult);
            if (updateItem != null)
            {
                updateItems.add(updateItem);
            }
        }

        if (Theme.social == theme)
        {
            final Grouper grouper = new DefaultGrouper();

            for (final UpdateItem updateItem : updateItems)
            {
                grouper.addUpdateItem(updateItem);
            }

            macroRenderContext.put("groupings", grouper.getUpdateItemGroupings());
        }
        else
        {
            macroRenderContext.put("updateItems", updateItems);
        }

        macroRenderContext.put("i18n", i18n);

        // ADVMACROS-273 - Mobile view support for recently-updated macro
        boolean mobile = false;
        if ("mobile".equals(conversionContext.getOutputDeviceType()))
        {
            mobile = true;
        }
        macroRenderContext.put("mobile", mobile);

        try
        {
            return renderRecentlyUpdated(theme, macroRenderContext);
        }
        catch (Exception e)
        {
            log.error("Error while trying to render the " + MACRO_NAME + " template.", e);
            throw new MacroExecutionException(e.getMessage());
        }
    }

    protected String renderRecentlyUpdated(final Theme theme, final Map<String, Object> macroRenderContext)
    {
        return VelocityUtils.getRenderedTemplate(getTemplate(theme), macroRenderContext);
    }

    protected String renderEmptyTemplate(final Map<String, Object> macroRenderContext)
    {
        final String emptyTemplate = "com/atlassian/confluence/plugins/macros/advanced/recentupdate/no-updates.vm";
        return VelocityUtils.getRenderedTemplate(emptyTemplate, macroRenderContext);
    }

    protected Map<String, Object> getMacroVelocityContext()
    {
        return defaultVelocityContext();
    }

    /**
     * HACK: Doing this because {@link RecentChangesSearchBuilder#getSpaceQuery} will not be able to resolve "@self"
     * because the builder does not have a reference to a page context.
     * <p>
     * Ideally, {@link com.atlassian.confluence.macro.query.params.SpaceKeyParameter} should provide a method to
     * translate this value, but it does not :( Instead it translates the value into a space query in {@link
     * com.atlassian.confluence.macro.query.params.SpaceKeyParameter.Interpreter#createSearchQuery} which we can't even
     * access because its protected.
     * <p>
     * {@link com.atlassian.confluence.macro.params.Parameter}'s, by design, has too many concerns.
     */
    private String getSpaceKeyParamValue(final Map<String, String> params, final ConversionContext conversionContext)
    {
        final String value = spaceKeyParam.getParameterValue(params);

        if (ContentFilteringMacro.DEFAULT_SPACE_KEY.equals(value) && conversionContext != null)
        {
            return conversionContext.getSpaceKey();
        }
        else
        {
            return value;
        }
    }

    private Integer getMaxResults(final MacroExecutionContext ctx, final I18NBean i18n) throws MacroExecutionException
    {
        try
        {
            return maxResultsParam.findValue(ctx);
        }
        catch (ParameterException pe)
        {
            throw new MacroExecutionException(i18n.getText("recently.updated.error.parse-max-results-param"));
        }
    }

    public boolean isInline()
    {
        return false;
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return NO_RENDER;
    }

    private Theme getTheme(final Map<String, String> params)
    {
        if (Theme.social.name().equals(params.get("theme")) || "true".equalsIgnoreCase(params.get("showProfilePic")))
        {
            return Theme.social;
        }
        else if (Theme.sidebar.name().equals(params.get("theme")))
        {
            return Theme.sidebar;
        }
        else // concise is the default theme
        {
            return Theme.concise;
        }
    }

    private static String getTemplate(final Theme theme)
    {
        return String.format("com/atlassian/confluence/plugins/macros/advanced/recentupdate/themes/%s/macro-template.vm", theme.name());
    }

    /**
     * Returns the status of the "hideHeading" parameter.  This parameter defaults to false and will be considered false
     * if set to "false" (ignoring case)
     *
     * @param params the parameter map
     * @return the boolean value of this parameter
     */
    private static boolean getHideHeading(final Map<String, String> params)
    {
        final String value = params.get("hideHeading");
        return (value != null && "true".equalsIgnoreCase(value));
    }

    public final String getName()
    {
        return MACRO_NAME;
    }

    /**
     * Returns type criteria that match the default behavior of the "types" parameter from the original recently-updated
     * macro.
     *
     * @return type criteria for the old default behavior
     */
    private static String getDefaultTypeParamValue()
    {
        StringBuilder value = new StringBuilder();
        EnumSet<ContentTypeEnum> types = EnumSet.allOf(ContentTypeEnum.class);

        //user status will be removed from core soon
        types.remove(ContentTypeEnum.USER_STATUS);
        // mail isn't in the default value
        types.remove(ContentTypeEnum.MAIL);
        value.append("-").append(ContentTypeEnum.MAIL.getRepresentation());

        for (ContentTypeEnum type : types)
        {
            value.append(",").append(type.getRepresentation());
        }

        return value.toString();
    }

    private static Map<String, String> getContentTypes()
    {
        final Map<String, String> result = new LinkedHashMap<String, String>();

        result.put(com.atlassian.confluence.pages.Page.CONTENT_TYPE, "content.type." + com.atlassian.confluence.pages.Page.CONTENT_TYPE);
        result.put(BlogPost.CONTENT_TYPE, "content.type." + BlogPost.CONTENT_TYPE);
        result.put(Comment.CONTENT_TYPE, "content.type." + Comment.CONTENT_TYPE);
        result.put(Attachment.CONTENT_TYPE, "content.type." + Attachment.CONTENT_TYPE);

        return result;
    }

    public void setContentTypesDisplayMapper(final ContentTypesDisplayMapper contentTypesDisplayMapper)
    {
        this.contentTypesDisplayMapper = contentTypesDisplayMapper;
    }

    public void setUserAccessor(final UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    public void setFormatSettingsManager(final FormatSettingsManager formatSettingsManager)
    {
        this.formatSettingsManager = formatSettingsManager;
    }

    public void setI18NBeanFactory(final I18NBeanFactory i18NBeanFactory)
    {
        this.i18NBeanFactory = i18NBeanFactory;
    }

    public void setLocaleManager(final LocaleManager localeManager)
    {
        this.localeManager = localeManager;
    }

    @Override
    public BodyType getBodyType()
    {
        return NONE;
    }

    @Override
    public OutputType getOutputType()
    {
        return BLOCK;
    }

    public void setContextPathHolder(ContextPathHolder contextPathHolder)
    {
        this.contextPathHolder = contextPathHolder;
    }
}
