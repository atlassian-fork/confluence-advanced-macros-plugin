package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;
import com.atlassian.confluence.security.access.annotations.RequiresAnyConfluenceAccess;

/**
 * This exists to serve AJAX requests for recent updates. Updates are served as separated results with no grouping.
 * At the moment, these are used to serve requests from the concise theme.
 */
@RequiresAnyConfluenceAccess
public class ConciseThemeChangesAction extends AbstractChangesAction
{
    protected Theme getTheme()
    {
        return Theme.concise;
    }
}
