package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

import java.util.List;
import java.util.Map;

import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.pagination.PageResponse;
import com.atlassian.confluence.api.model.pagination.SimplePageRequest;
import com.atlassian.confluence.api.model.search.SearchContext;
import com.atlassian.confluence.api.service.search.CQLSearchService;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentEntityManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.ContentFilteringMacro;
import com.atlassian.confluence.macro.MacroExecutionContext;
import com.atlassian.confluence.macro.params.ParameterException;
import com.atlassian.confluence.plugins.macros.advanced.analytics.LabelledContentMacroMetrics;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.AdvancedMacrosExcerpter;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.ExcerptType;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerManager;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import org.apache.commons.lang.StringUtils;

public class LabelledContentMacro extends ContentFilteringMacro
{
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/macros/advanced/contentbylabel/labelledcontent.vm";

    private static final String SPACE_ALL = "@all";

    private ConfluenceActionSupport confluenceActionSupport;
    private WebResourceManager webResourceManager;
    private ContentEntityManager contentEntityManager;
    private AdvancedMacrosExcerpter advancedMacrosExcerpter;
    private EventPublisher eventPublisher;

    public LabelledContentMacro()
    {
        super();
        // parse the space(s) parameter
        spaceKeyParam.addParameterAlias("key");
        spaceKeyParam.setDefaultValue(SPACE_ALL);

        maxResultsParam.addParameterAlias("maxResults");
        maxResultsParam.setDefaultValue(ContentFilteringMacro.DEFAULT_MAX_RESULTS);
    }

    public boolean isInline()
    {
        return false; 
    }

    public boolean hasBody()
    {
        return false;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    @Override
    public String execute(MacroExecutionContext ctx) throws MacroException
    {
        final LabelledContentMacroMetrics.Builder metrics = LabelledContentMacroMetrics.builder();
        Map<String, String> parameters = ctx.getParams();

        String cql = parameters.get("cql");
        cql += buildOrderByClause(ctx);

        Map<String, Object> contextMap = getMacroVelocityContext();
        contextMap.putAll(makeRenderContext(ctx, parameters, cql, metrics));
        
        webResourceManager.requireResource("confluence.macros.advanced:content-by-label-resources");

        try
        {
            metrics.templateRenderStart();
            return render(contextMap);
        }
        finally
        {
            metrics.templateRenderFinish()
                    .publish(eventPublisher);
        }
    }

    @VisibleForTesting
    Map<String, Object> makeRenderContext(MacroExecutionContext ctx, Map<String, String> parameters, String cql, final LabelledContentMacroMetrics.Builder metrics) throws MacroException
    {
        SimplePageRequest pageRequest = buildPageRequest(ctx);

        SearchContext searchContext = ctx.getPageContext().toSearchContext().build();

        metrics.contentSearchStart(pageRequest);
        PageResponse<Content> response = getSearchService().searchContent(cql, searchContext, pageRequest);
        metrics.contentSearchFinish(response);

        // Nasty, but we're not rewriting the templates just now - and they expect ContentEntityObjects.
        metrics.fetchContentEntitiesStart();
        List<ContentEntityObject> contents = asContentEntityObjects(response);
        metrics.fetchContentEntitiesFinish();

        String title = parameters.get("title");

        // CONF-12749 Limit the label links to a single space if we only search a single space
        boolean limitLabelLinksToSpace = cql.contains("space = ");

        Map<String, Object> contextMap = Maps.newHashMap();
        contextMap.put("title", title);
        contextMap.put("contents", contents);
        contextMap.put("showLabels", getBooleanParameter(parameters.get("showLabels"), true));
        contextMap.put("showSpace", getBooleanParameter(parameters.get("showSpace"), true));
        contextMap.put("limitLabelLinksToSpace", limitLabelLinksToSpace);
        contextMap.put("excerptType", ExcerptType.fromString(parameters.get("excerptType")));
        contextMap.put("excerpter", advancedMacrosExcerpter);

        return contextMap;
    }

    private String buildOrderByClause(MacroExecutionContext ctx) throws MacroException
    {
        SearchSort searchSort;
        try
        {
            searchSort = sortParam.findValue(ctx);
        }
        catch (ParameterException pe)
        {
            throw new MacroException(
                    getConfluenceActionSupport().getText("contentbylabel.error.parse-reverse-or-sort-param"),
                    pe);
        }

        if (searchSort == null)
            return "";

        String direction = searchSort.getOrder() == SearchSort.Order.DESCENDING ? " desc" : "";

        String key = searchSort.getKey();
        if (key.equals("modified"))
            key = "lastModified";  // FIXME - bad, bad, bad. Update SearchSort with a getCQLKey() method?

        return " order by " + key + direction;
    }

    private SimplePageRequest buildPageRequest(MacroExecutionContext ctx) throws MacroException
    {
        Integer maxResults;
        try
        {
            maxResults = maxResultsParam.findValue(ctx);
        }
        catch (ParameterException pe)
        {
            throw new MacroException(
                    getConfluenceActionSupport().getText("contentbylabel.error.parse-max-labels-param"),
                    pe);
        }
        if (maxResults == 0)
            maxResults = 15;

        return new SimplePageRequest(0, maxResults);
    }

    private List<ContentEntityObject> asContentEntityObjects(PageResponse<Content> response)
    {
        List<Content> results = response.getResults();
        List<ContentEntityObject> ceos = Lists.newArrayList();
        for (Content result : results)
        {
            ceos.add(contentEntityManager.getById(result.getId().asLong()));
        }
        return ceos;
    }

    private CQLSearchService searchService;

    private CQLSearchService getSearchService()
    {
        if (searchService == null)
        {
            // HACK - if there's an already-used better way to get a v2 plugin component in here, please refactor! dT
            OsgiContainerManager osgiContainerManager = (OsgiContainerManager) ContainerManager.getComponent("osgiContainerManager");
            setSearchService((CQLSearchService) osgiContainerManager.getServiceTracker(CQLSearchService.class.getName()).getService());
        }
        return searchService;
    }

    public void setSearchService(CQLSearchService searchService)
    {
        this.searchService = searchService;
    }

/// CLOVER:OFF
    @SuppressWarnings("unchecked")
    protected Map<String, Object> getMacroVelocityContext()
    {
        // current MacroUtils returns untyped map, but its keys are always
        // strings and we don't want to restrict what it can hold        
        return MacroUtils.defaultVelocityContext();
    }

    protected String render(Map<String, Object> contextMap)
    {
        return VelocityUtils.getRenderedTemplate(TEMPLATE_NAME, contextMap);
    }
///CLOVER:ON
    
    private Boolean getBooleanParameter(String booleanValue, boolean defaultValue)
    {
        if (StringUtils.isNotBlank(booleanValue))
            return Boolean.valueOf(booleanValue);
        else
            return defaultValue;
    }

    public void setContentEntityManager(ContentEntityManager contentEntityManager)
    {
        this.contentEntityManager = contentEntityManager;
    }

    protected ConfluenceActionSupport getConfluenceActionSupport()
    {
        if (null == confluenceActionSupport)
            confluenceActionSupport = GeneralUtil.newWiredConfluenceActionSupport();
        return confluenceActionSupport;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    public void setAdvancedMacrosExcerpter(AdvancedMacrosExcerpter advancedMacrosExcerpter)
    {
        this.advancedMacrosExcerpter = advancedMacrosExcerpter;
    }

    public void setEventPublisher(final EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }
}
