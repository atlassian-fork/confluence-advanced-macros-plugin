package com.atlassian.confluence.plugins.macros.advanced.contentbylabel;

/**
 * A no-op QueryExpression implementation that we use instead of null references.
 */
public class EmptyQueryExpression implements QueryExpression
{
    public static final EmptyQueryExpression EMPTY = new EmptyQueryExpression();

    private EmptyQueryExpression()
    {
    }

    @Override
    public String toQueryString()
    {
        return "";
    }
}
