package com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.FormatSettingsManager;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUserPreferences;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;

import java.util.List;
import java.util.Map;

/**
 * Show the 'n' most recently modified labels. The number of labels returned by this macro is limited to 100.
 *
 * This macro supports the three scopes: global, space and personal.
 *
 * This macro supports two styles. Compact and tablular. Compact will render a simple comma separated list and tabular
 * will render a table of information related to the recent label usage. In particular, the name of the user that added
 * the label to the content, and the page to which it was added.
 *
 */
public class RecentlyUsedLabelsMacro extends BaseMacro
{
    private static final String TEMPLATE_NAME = "com/atlassian/confluence/plugins/macros/advanced/recentlyusedlabels";

    private static final String TABULAR_TEMPLATE_NAME = TEMPLATE_NAME + "-tabular.vm";

    private static final String COMPACT_TEMPLATE_NAME = TEMPLATE_NAME + "-compact.vm";

    private static final String STYLE = "style";

    private static final String COUNT = "count";
    private static final int DEFAULT_COUNT = 10;

    private static final String SCOPE = "scope";
    private static final String DEFAULT_SCOPE = "global";

    private static final String TITLE = "title";

    private static final int MAX_RESULTS = 100;

    private LabelManager labelManager;

    private UserAccessor userAccessor;

    private FormatSettingsManager formatSettingsManager;

    private LocaleManager localeManager;

    public void setLabelManager(LabelManager manager)
    {
        this.labelManager = manager;
    }

    public void setUserAccessor(UserAccessor userAccessor)
    {
        this.userAccessor = userAccessor;
    }

    public void setFormatSettingsManager(FormatSettingsManager formatSettingsManager)
    {
        this.formatSettingsManager = formatSettingsManager;
    }

    public void setLocaleManager(final LocaleManager localeManager)
    {
        this.localeManager = localeManager;
    }

    public String execute(Map parameters, String string, RenderContext renderContext) throws MacroException
    {
        Map<String, Object> contextMap = getMacroVelocityContext();

        // initialise parameters.
        int maxResults = DEFAULT_COUNT;
        if (parameters.containsKey(COUNT))
        {
            maxResults = Integer.parseInt((String) parameters.get(COUNT));
        }

        maxResults = (maxResults > MAX_RESULTS) ? MAX_RESULTS : maxResults;

        String scope = DEFAULT_SCOPE;
        if (TextUtils.stringSet((String) parameters.get(SCOPE)))
        {
            scope = (String) parameters.get(SCOPE);
        }

        String title;
        if (TextUtils.stringSet((String) parameters.get(TITLE)))
        {
            title = (String)parameters.get(TITLE);
            contextMap.put("title", title);
        }

        User user = AuthenticatedUserThreadLocal.getUser();
        String spaceKey = ((PageContext)renderContext).getSpaceKey();

        String template = COMPACT_TEMPLATE_NAME;
        if (TextUtils.stringSet((String) parameters.get(STYLE)))
        {
            String style = (String)parameters.get(STYLE);
            if ("table".equalsIgnoreCase(style))
            {
                template = TABULAR_TEMPLATE_NAME;
            }
        }

        if(TABULAR_TEMPLATE_NAME.equals(template))
        {
            contextMap.put("recentlyUsedLabellings", getLabellings(spaceKey, scope, user, maxResults));
        }
        else
        {
            contextMap.put("recentlyUsedLabels", getLabels(spaceKey, scope, user, maxResults));
        }

        ConfluenceUserPreferences pref = userAccessor.getConfluenceUserPreferences(user);
        DateFormatter df = pref.getDateFormatter(formatSettingsManager, localeManager);
        contextMap.put("dateFormatter", df);
        contextMap.put("spaceKey", spaceKey);
        contextMap.put("req", ServletActionContext.getRequest());
        contextMap.put("generalUtil", new GeneralUtil());
        contextMap.put("webwork", new GeneralUtil());
        return renderRecentlyUsedLabels(contextMap, template);
    }

    ///CLOVER:OFF
    protected String renderRecentlyUsedLabels(Map<String, Object> contextMap, String template)
    {
        return VelocityUtils.getRenderedTemplate(template, contextMap);
    }

    protected Map<String, Object> getMacroVelocityContext()
    {
        return MacroUtils.defaultVelocityContext();
    }

    ///CLOVER:ON

    private List getLabels(String spaceKey, String scope, User user, int maxResults)
    {
        List labels;
        if (scope.equalsIgnoreCase("space"))
        {
            labels = labelManager.getRecentlyUsedLabelsInSpace(spaceKey, maxResults);
        }
        else if (scope.equalsIgnoreCase("personal"))
        {
            if (user != null)
                labels = labelManager.getRecentlyUsedPersonalLabels(user.getName(), maxResults);
            else
                labels = labelManager.getRecentlyUsedLabels(maxResults);
        }
        else
        {
            labels = labelManager.getRecentlyUsedLabels(maxResults);
        }
        return labels;
    }

    private List getLabellings(String spaceKey, String scope, User user, int maxResults)
    {
        List labellings;
        if (scope.equalsIgnoreCase("space"))
        {
            labellings = labelManager.getRecentlyUsedLabellingsInSpace(spaceKey, maxResults);
        }
        else if (scope.equalsIgnoreCase("personal"))
        {
            if (user != null)
                labellings = labelManager.getRecentlyUsedPersonalLabellings(user.getName(), maxResults);
            else
                labellings = labelManager.getRecentlyUsedLabellings(maxResults);
        }
        else
        {
            labellings = labelManager.getRecentlyUsedLabellings(maxResults);
        }
        return labellings;
    }

    public RenderMode getBodyRenderMode()
    {
        return RenderMode.NO_RENDER;
    }

    public boolean hasBody()
    {
        return false;
    }

    public boolean isInline()
    {
        return false;
    }
}

