package com.atlassian.confluence.plugins.macros.advanced.recentupdate.ajax;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContextPathHolder;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.DefaultUpdateItemFactory;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.RecentChangesSearchBuilder;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.Theme;
import com.atlassian.confluence.plugins.macros.advanced.recentupdate.UpdateItem;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchTokenExpiredException;
import com.atlassian.confluence.search.v2.SearchWithToken;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public abstract class AbstractChangesAction extends ConfluenceActionSupport
{
    private static final Logger log = LoggerFactory.getLogger(AbstractChangesAction.class);

    private SearchManager searchManager;
    private ContextPathHolder contextPathHolder;

    private String authors;
    private String contentType;
    private String spaceKeys;
    private String labels;

    private ContentTypesDisplayMapper contentTypesDisplayMapper;

    private int startIndex;
    private int pageSize;
    private long searchToken;

    private String nextPageUrl;

    List<UpdateItem> updateItems;

    @Override public String execute() throws Exception
    {
        RecentChangesSearchBuilder searchBuilder = new RecentChangesSearchBuilder()
            .withLabels(labels).withAuthors(authors).withContentTypes(contentType).withSpaceKeys(spaceKeys);

        if (searchToken > 0)
            searchBuilder.withSearchToken(searchToken);

        if (startIndex >= 0)
            searchBuilder.withStartIndex(startIndex);

        if (pageSize > 0)
            searchBuilder.withPageSize(pageSize);

        SearchResults searchResults;

        if (searchToken > 0)
        {
            try
            {
                searchResults = searchManager.search(searchBuilder.buildSearchWithToken());
            }
            catch (SearchTokenExpiredException e)
            {
                addActionError(getText("recently.updated.search.token.expired"));

                return INPUT;
            }
            catch (InvalidSearchException e)
            {
                log.debug("Invalid search", e);
                addActionError(e.getMessage());

                return INPUT;
            }
        }
        else
        {
            try
            {
                searchResults = searchManager.search(searchBuilder.buildSearch());
            }
            catch (InvalidSearchException e)
            {
                log.debug("Invalid search", e);
                addActionError(e.getMessage());

                return INPUT;
            }
        }

        if (!searchResults.isLastPage())
        {
            SearchWithToken nextPageSearch = searchResults.getNextPageSearch();

            searchBuilder.withStartIndex(nextPageSearch.getStartOffset()).withPageSize(nextPageSearch.getLimit()).withSearchToken(nextPageSearch.getSearchToken());

            nextPageUrl = searchBuilder.buildSearchUrl(getTheme(), contextPathHolder.getContextPath());
        }

        DefaultUpdateItemFactory updateItemFactory = new DefaultUpdateItemFactory(getDateFormatter(), getI18n(), contentTypesDisplayMapper);
        updateItems = new LinkedList<UpdateItem>();
        for (SearchResult searchResult : searchResults)
        {
            UpdateItem updateItem = updateItemFactory.get(searchResult);
            if (updateItem != null)
                updateItems.add(updateItem);
        }

        return super.execute();
    }

    protected abstract Theme getTheme();

    public void setAuthors(String authors)
    {
        this.authors = authors;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public void setSearchManager(SearchManager searchManager)
    {
        this.searchManager = searchManager;
    }

    public void setContentType(String contentType)
    {
        this.contentType = contentType;
    }

    public void setSpaceKeys(String spaceKeys)
    {
        this.spaceKeys = spaceKeys;
    }

    public void setLabels(String labels)
    {
        this.labels = labels;
    }

    public String getNextPageUrl()
    {
        return nextPageUrl;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public List<UpdateItem> getUpdateItems()
    {
        return updateItems;
    }

    public void setContentTypesDisplayMapper(ContentTypesDisplayMapper contentTypesDisplayMapper)
    {
        this.contentTypesDisplayMapper = contentTypesDisplayMapper;
    }

    public void setSearchToken(long searchToken)
    {
        this.searchToken = searchToken;
    }

    public void setContextPathHolder(ContextPathHolder contextPathHolder)
    {
        this.contextPathHolder = contextPathHolder;
    }
}
