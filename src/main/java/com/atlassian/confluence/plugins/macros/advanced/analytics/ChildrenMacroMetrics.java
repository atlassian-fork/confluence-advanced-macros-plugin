package com.atlassian.confluence.plugins.macros.advanced.analytics;

import com.atlassian.analytics.api.annotations.EventName;
import com.atlassian.confluence.plugins.macros.advanced.xhtml.ExcerptType;

import com.google.common.base.Ticker;

import org.joda.time.Duration;

@EventName("confluence.macro.metrics.children")
public class ChildrenMacroMetrics
{
    private final Duration permittedChildrenFetchTimer;
    private final Duration excerptSumariseTimer;
    private final Duration filterPermittedEntitiesTimer;
    private final Duration renderPageLinkTimer;
    private final int permittedChildrenFetchItemTotal;
    private final int permittedChildrenFetchInvocationCount;
    private final int filterPermittedEntitiesItemTotal;
    private final int filterPermittedEntitiesInvocationCount;
    private final int renderPageLinkInvocationCount;
    private final int excerptSummariseInvocationCount;
    private final ExcerptType excerptType;
    private final int depth;

    ChildrenMacroMetrics(final Duration permittedChildrenFetchTimer,
            final Duration excerptSumariseTimer,
            final Duration filterPermittedEntitiesTimer,
            final Duration renderPageLinkTimer,
            final int permittedChildrenFetchItemTotal,
            final int permittedChildrenFetchInvocationCount,
            final int filterPermittedEntitiesItemTotal,
            final int filterPermittedEntitiesInvocationCount,
            final int renderPageLinkInvocationCount,
            final int excerptSummariseInvocationCount,
            final ExcerptType excerptType,
            final int depth)
    {
        this.permittedChildrenFetchTimer = permittedChildrenFetchTimer;
        this.excerptSumariseTimer = excerptSumariseTimer;
        this.filterPermittedEntitiesTimer = filterPermittedEntitiesTimer;
        this.renderPageLinkTimer = renderPageLinkTimer;
        this.permittedChildrenFetchItemTotal = permittedChildrenFetchItemTotal;
        this.permittedChildrenFetchInvocationCount = permittedChildrenFetchInvocationCount;
        this.filterPermittedEntitiesItemTotal = filterPermittedEntitiesItemTotal;
        this.filterPermittedEntitiesInvocationCount = filterPermittedEntitiesInvocationCount;
        this.renderPageLinkInvocationCount = renderPageLinkInvocationCount;
        this.excerptSummariseInvocationCount = excerptSummariseInvocationCount;
        this.excerptType = excerptType;
        this.depth = depth;
    }

    public long getPermittedChildrenFetchMillis()
    {
        return permittedChildrenFetchTimer.getMillis();
    }

    public long getExcerptSumariseMillis()
    {
        return excerptSumariseTimer.getMillis();
    }

    public long getFilterPermittedEntitiesMillis()
    {
        return filterPermittedEntitiesTimer.getMillis();
    }

    public long getRenderPageLinkMillis()
    {
        return renderPageLinkTimer.getMillis();
    }

    public int getPermittedChildrenFetchItemTotal()
    {
        return permittedChildrenFetchItemTotal;
    }

    public int getFilterPermittedEntitiesItemTotal()
    {
        return filterPermittedEntitiesItemTotal;
    }

    public int getPermittedChildrenFetchInvocationCount()
    {
        return permittedChildrenFetchInvocationCount;
    }

    public int getFilterPermittedEntitiesInvocationCount()
    {
        return filterPermittedEntitiesInvocationCount;
    }

    public int getRenderPageLinkInvocationCount()
    {
        return renderPageLinkInvocationCount;
    }

    public int getExcerptSummariseInvocationCount()
    {
        return excerptSummariseInvocationCount;
    }

    public String getExcerptType()
    {
        return excerptType.getValue();
    }

    public int getDepth()
    {
        return depth;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private final Ticker ticker = Ticker.systemTicker();

        private final Timer permittedChildrenFetchTimer = new Timer(ticker);
        private final Timer excerptSumariseTimer = new Timer(ticker);
        private final Timer filterPermittedEntitiesTimer = new Timer(ticker);
        private final Timer renderPageLinkTimer = new Timer(ticker);

        private int permittedChildrenFetchItemTotal;
        private int permittedChildrenFetchInvocationCount;
        private int filterPermittedEntitiesItemTotal;
        private int filterPermittedEntitiesInvocationCount;
        private int excerptSummariseInvocationCount;
        private int renderPageLinkInvocationCount;

        private ExcerptType excerptType;
        private int depth;

        public ChildrenMacroMetrics build()
        {
            return new ChildrenMacroMetrics(
                    permittedChildrenFetchTimer.duration(),
                    excerptSumariseTimer.duration(),
                    filterPermittedEntitiesTimer.duration(),
                    renderPageLinkTimer.duration(),
                    permittedChildrenFetchItemTotal,
                    permittedChildrenFetchInvocationCount,
                    filterPermittedEntitiesItemTotal,
                    filterPermittedEntitiesInvocationCount,
                    renderPageLinkInvocationCount,
                    excerptSummariseInvocationCount,
                    excerptType, depth);
        }

        public void permittedChildrenFetchStart()
        {
            permittedChildrenFetchTimer.start();
        }

        public void permittedChildrenFetchFinish(final int size)
        {
            permittedChildrenFetchTimer.stop();
            permittedChildrenFetchItemTotal += size;
            permittedChildrenFetchInvocationCount++;
        }

        public void excerptSummariseStart()
        {
            excerptSumariseTimer.start();
        }

        public void excerptSummariseFinish()
        {
            excerptSumariseTimer.stop();
            excerptSummariseInvocationCount++;
        }

        public void filterPermittedEntitiesStart(final int size)
        {
            filterPermittedEntitiesTimer.start();
            filterPermittedEntitiesItemTotal += size;
            filterPermittedEntitiesInvocationCount++;
        }

        public void filterPermittedEntitiesFinish()
        {
            filterPermittedEntitiesTimer.stop();
        }

        public void renderPageLinkStart()
        {
            renderPageLinkTimer.start();
        }

        public void renderPageLinkFinish()
        {
            renderPageLinkTimer.stop();
            renderPageLinkInvocationCount++;
        }

        public void renderOptions(final ExcerptType excerptType, final int depth)
        {
            this.excerptType = excerptType;
            this.depth = depth;
        }
    }
}
