package it.webdriver.com.atlassian.confluence.plugins.macro.advanced;

import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.labels.LabelableType;
import com.atlassian.confluence.pageobjects.component.dialog.MacroBrowserDialog;
import com.atlassian.confluence.pageobjects.component.dialog.MacroBrowserPreview;
import com.atlassian.confluence.pageobjects.component.form.CqlComponent;
import com.atlassian.confluence.pageobjects.component.form.MultiLabelPicker;
import com.atlassian.confluence.pageobjects.component.form.MultiSpacePicker;
import com.atlassian.confluence.util.test.annotations.ExportedTestClass;
import com.atlassian.confluence.webdriver.AbstractEditorWebDriverTest;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * Tests that the contentbylabel macro is using the CQL UI component correctly.
 *
 * Further tests for contentbylabel macro functionality are in LabelledContentMacroFuncTest, while
 * further tests for CQL UI are in Confluence core.
 */
@ExportedTestClass
public class LabelledContentMacroWebDriverTest extends AbstractEditorWebDriverTest
{
    public static final String MACRO_NAME = "contentbylabel";

    private MacroBrowserDialog mb;

    private CqlComponent cqlComponent;

    private static Page createdPage;
    private static Page createdPageInDifferentSpace;

    @BeforeClass
    public static void addPagesAndLabels()
    {
        rpc.labels.addLabel("foo", Page.TEST);
        rpc.labels.addLabel("baz", Page.TEST);

        Page page = new Page(Space.TEST, "Test page 1", "Test page content");
        createdPage = rpc.content.createPage(page, ContentRepresentation.PLAIN);
        rpc.labels.addLabel("foo", createdPage);
        rpc.labels.addLabel("bar", createdPage);
        rpc.labels.addLabel("baz", createdPage);

        Space space = new Space("SS", "Second Space");
        Space secondSpace = rpc.spaces.create(space, false);
        Page page2 = new Page(secondSpace, "Test page 2", "Test page content");
        createdPageInDifferentSpace = rpc.content.createPage(page2, ContentRepresentation.PLAIN);
        rpc.labels.addLabel("foo", createdPageInDifferentSpace);

        rpc.flushIndexQueue();
    }

    @Before
    public void setUp()
    {
        editorPage.getEditor().getContent().focus();
        mb = editorPage.getEditor().openMacroBrowser();
        cqlComponent = mb.selectMacro(MACRO_NAME).getCqlComponent();
    }

    @Test
    public void testLabelOnly() throws Exception
    {
        MacroBrowserPreview preview = selectLabelsAndPreview("foo");
        assertThat(preview.getText(), containsString(Page.TEST.getTitle()));
    }

    @Test
    public void testLabelSimpleExclude() throws Exception
    {
        MacroBrowserPreview preview = selectLabelsAndPreview("-bar");
        assertThat(preview.getText(), containsString(Page.TEST.getTitle()));
        assertThat(preview.getText(), not(containsString(createdPage.getTitle())));
    }

    @Test
    public void testLabelExcludeWithLabelInclude() throws Exception
    {
        String [] labels = { "-bar", "foo" };
        MacroBrowserPreview preview = selectLabelsAndPreview(labels);
        assertThat(preview.getText(), containsString(Page.TEST.getTitle()));
        assertThat(preview.getText(), not(containsString(createdPage.getTitle())));
    }

    @Test
    public void testLabelExcludeWithTwoLabelFields() throws Exception
    {
        String[] labels = { "-bar" };
        String[] labels2 = { "foo" };
        MacroBrowserPreview preview = selectLabelsInTwoFieldsAndPreview(labels, labels2);
        assertThat(preview.getText(), containsString(Page.TEST.getTitle()));
        assertThat(preview.getText(), not(containsString(createdPage.getTitle())));
    }

    @Test
    public void testLabelIncludeAndExcludeWithTwoLabelFields() throws Exception
    {
        String[] labels = { "-bar", "foo" };
        String[] labels2 = { "baz" };
        MacroBrowserPreview preview = selectLabelsInTwoFieldsAndPreview(labels, labels2);
        assertThat(preview.getText(), containsString(Page.TEST.getTitle()));
        assertThat(preview.getText(), not(containsString(createdPage.getTitle())));
    }

    @Test
    public void testSpaceSimpleExclude() throws Exception
    {
        MacroBrowserPreview preview = selectLabelAndSpaceThenPreview("foo", "-" + Space.TEST.getName());
        assertThat(preview.getText(), containsString(createdPageInDifferentSpace.getTitle()));
        assertThat(preview.getText(), not(containsString(Page.TEST.getTitle())));
    }

    @Test
    public void testTypeSimpleExclude() throws Exception
    {
        Attachment attachment = addAttachment("just_a_text_file1");
        MacroBrowserPreview preview = selectLabelAndTypeThenPreview("foo", "-Page");
        assertThat(preview.getText(), containsString(attachment.getTitle()));
        assertThat(preview.getText(), not(containsString(Page.TEST.getTitle())));
    }

    @Test
    public void testAttachmentWithLabel() throws Exception
    {
        Attachment attachment = addAttachment("just_a_text_file2");
        MacroBrowserPreview preview = selectLabelsAndPreview("foo");
        assertThat(preview.getText(), containsString(attachment.getTitle()));
    }

    private Attachment makeAttachment(String attachmentName)
    {
        return new Attachment.AttachmentBuilder()
                .contentType("text/plain")
                .comment("")
                .minorEdit(false)
                .id(0)
                .version(0)
                .contentEntity(Page.TEST)
                .filename(attachmentName)
                .data(new byte[] {1,2,3})
                .build();
    }

    private Attachment addAttachment(String attachmentName)
    {
        Attachment attachment = makeAttachment(attachmentName);
        attachment = rpc.attachments.addAttachment(createdPage.getContentId(), attachment);
        rpc.labels.addLabelToLabelable(attachment.getId(), LabelableType.CONTENT, "foo");
        rpc.labels.addLabelToLabelable(attachment.getId(), LabelableType.CONTENT, "bar");
        rpc.flushIndexQueue();
        return attachment;
    }

    private MacroBrowserPreview selectLabelsAndPreview(String... labelNames)
    {
        selectLabels(labelNames);
        return mb.preview();
    }

    private MacroBrowserPreview selectLabelsInTwoFieldsAndPreview(String[] labelNames, String[] labelNames2)
    {
        selectLabels(labelNames);
        cqlComponent.addFilter("Label");
        MultiLabelPicker secondLabelPicker = cqlComponent.getLabelField("label", 1);
        selectLabelsInPicker(secondLabelPicker, labelNames2);
        return mb.preview();
    }

    private MacroBrowserPreview selectLabelAndSpaceThenPreview(String labelName, String spaceName)
    {
        selectLabels(labelName);
        cqlComponent.addFilter("In space");
        MultiSpacePicker spacePicker = cqlComponent.getSpaceField("space");
        spacePicker.search(spaceName);
        spacePicker.selectResultText(spaceName);
        return mb.preview();
    }

    private MacroBrowserPreview selectLabelAndTypeThenPreview(String labelName, String type)
    {
        selectLabels(labelName);
        cqlComponent.addFilter("Of type");
        MultiSpacePicker spacePicker = cqlComponent.getSpaceField("type");
        spacePicker.search(type);
        spacePicker.selectResultText(type);
        return mb.preview();
    }

    private void selectLabels(String... labelNames)
    {
        MultiLabelPicker labelPicker = cqlComponent.getLabelField("label");
        selectLabelsInPicker(labelPicker, labelNames);
    }

    private void selectLabelsInPicker(MultiLabelPicker labelPicker, String... labelNames)
    {
        for (String labelName : labelNames)
        {
            labelPicker.searchAndSelectLabel(labelName);
        }
    }
}
