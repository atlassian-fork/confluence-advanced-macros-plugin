package it.com.atlassian.confluence.plugins.macros.advanced;

import com.atlassian.confluence.it.AcceptanceTestHelper;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.it.user.LoginHelper;

import org.junit.Before;
import org.junit.rules.TestName;

import static com.atlassian.confluence.it.content.ViewContentBean.viewPage;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertTextPresent;

public class BaseExcerptsTestCase
{
    protected static final String EXCERPT_TEXT = "This is an excerpt";
    protected static final String P_EXCERPT_TEXT_P = "<p>" + EXCERPT_TEXT + "</p>";

    protected static final String GRAND_CHILD_PAGE_NAME = "grandPage";
    protected static final String CHILD_PAGE_NAME = "childPage";
    protected static final String PARENT_PAGE_NAME = "parentPage";
    protected static final String LABEL = "mylabel";
    protected static final String GRAND_CHILD_PAGE_CONTENT = "grand child page";

    protected AcceptanceTestHelper helper = AcceptanceTestHelper.make();
    protected TestName testName = new TestName();
    protected ConfluenceRpc rpc;
    protected LoginHelper loginHelper;

    @Before
    public void setUp() throws Exception
    {
        helper.setUp(getClass(), testName);
        rpc = helper.getRpc();
        rpc.logIn(User.ADMIN);
        loginHelper = new LoginHelper(helper.getWebTester());
    }

    protected void excerptSetupAndBasicChecks(long parentId)
    {
        Page childrenPage = new Page(Space.TEST, CHILD_PAGE_NAME,
                "<ac:structured-macro ac:name=\"excerpt\">"
                        + "<ac:parameter ac:name=\"atlassian-macro-output-type\">INLINE</ac:parameter>"
                        + "<ac:rich-text-body><p>" + EXCERPT_TEXT + "</p></ac:rich-text-body>"
                + "</ac:structured-macro>", parentId);
        long childId = rpc.createPage(childrenPage);
        rpc.addLabel(LABEL, childrenPage);

        Page grandChildrenPage = new Page(Space.TEST, GRAND_CHILD_PAGE_NAME, GRAND_CHILD_PAGE_CONTENT, childId);
        rpc.createPage(grandChildrenPage);
        rpc.addLabel("mylabel", grandChildrenPage);

        rpc.flushIndexQueue();

        loginHelper.logInAs(User.TEST);

        viewPage(Space.TEST, PARENT_PAGE_NAME);

        // Now check that child (and its excerpt) and grand child page appears in the parent page from use of children macro
        assertTextPresent(CHILD_PAGE_NAME);
        assertTextPresent(EXCERPT_TEXT);
        assertTextPresent(GRAND_CHILD_PAGE_NAME);
    }
}
