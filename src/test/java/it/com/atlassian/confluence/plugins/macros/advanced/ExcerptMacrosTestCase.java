package it.com.atlassian.confluence.plugins.macros.advanced;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.atlassian.confluence.pages.BlogPost;

public class ExcerptMacrosTestCase extends AbstractConfluencePluginWebTestCaseBase
{
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("/yyyy/MM/dd/");
    
    public void testExcerptHidden()
    {
        viewPageById(
                createPage(
                        TEST_SPACE_KEY,
                        "testExcerptHidden",
                        "{excerpt:hidden=true}Hidden excerpt{excerpt}"
                )
        );

        assertTextNotPresent("Hidden excerpt");
    }

    public void testExcerptNotHiddenByDefault()                                                                                                                                   
    {
        viewPageById(
                createPage(
                        TEST_SPACE_KEY,
                        "testExcerptNotHiddenByDefault",
                        "{excerpt:hidden=true}Excerpt text{excerpt}"
                )
        );

        assertTextNotPresent("Excerpt text");
    }

    public void testIncludeExcerptFromBlogPost()
    {
        Date blogPostPublishedDate = new Date();

        String blogTitle = "blogPostWithExcerpt";
        String excerptText = "Excerpt text";

        long blogPostId = createBlogPost(TEST_SPACE_KEY, blogTitle, "{excerpt}" + excerptText + "{excerpt}", blogPostPublishedDate);
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeExcerptFromBlogPost", "{excerpt-include:" + DATE_FORMAT.format(blogPostPublishedDate) + blogTitle + "}");

        viewPageById(blogPostId); /* Seems like excerpt is only stored on macro render? */
        viewPageById(testPageId);

        assertEquals(blogTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
        assertEquals(excerptText, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
    }

    public void testIncludeExcerptFromPage()
    {
        String pageWithExcerptTitle = "pageWithExcerpt";
        String excerptText = "Excerpt text";

        long pageWithExcerptId = createPage(TEST_SPACE_KEY, pageWithExcerptTitle, "{excerpt}" + excerptText + "{excerpt}");
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeExcerptFromPage", "{excerpt-include:" + pageWithExcerptTitle + "}");

        viewPageById(pageWithExcerptId); /* Seems like excerpt is only stored on macro render? */
        viewPageById(testPageId);

        assertEquals(pageWithExcerptTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
        assertEquals(excerptText, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
    }

	public void testIncludeExcerptWithNoPanel()
    {
        String pageWithExcerptTitle = "pageWithExcerpt";
        String excerptText = "Excerpt text";

        long pageWithExcerptId = createPage(TEST_SPACE_KEY, pageWithExcerptTitle, "{excerpt}" + excerptText + "{excerpt}");
        long testPageId = createPage(TEST_SPACE_KEY, "testIncludeExcerptFromPage", "{excerpt-include:" + pageWithExcerptTitle + "|nopanel=true}");

        viewPageById(pageWithExcerptId); /* Seems like excerpt is only stored on macro render? */
        viewPageById(testPageId);

        assertElementNotPresentByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]");
        assertEquals(excerptText, getElementTextByXPath("//div[@class='wiki-content']"));
    }

	public void testCircularReference()
	{
		long pageD = createPage(TEST_SPACE_KEY, "D", "");
		long blogPostE = createBlogPost(TEST_SPACE_KEY, "E", "{excerpt}bananas{excerpt-include:F|nopanel=true}{excerpt}");
		long pageF = createPage(TEST_SPACE_KEY, "F", "{excerpt}pears{excerpt-include:D|nopanel=true}{excerpt}");

        updatePage(pageD, "{excerpt}apples{excerpt-include:" + getBlogpostWikiValue("E") +"|nopanel=true}{excerpt}");

		viewPageById(pageD);
		assertTextsPresentInOrder(new String[]{"apples", "bananas", "pears", "apples"});
		assertTextPresent("Unable to render excerpt. Already included page E.");
		viewPageById(blogPostE);
        assertTextsPresentInOrder(new String[]{"bananas", "pears", "apples", "bananas"});
		assertTextPresent("Unable to render excerpt. Already included page F.");
		viewPageById(pageF);
        assertTextsPresentInOrder(new String[]{"pears", "apples", "bananas", "pears"});
		assertTextPresent("Unable to render excerpt. Already included page D.");
	}

	public void testCircularReferenceWithPanels()
	{
		long pageD = createPage(TEST_SPACE_KEY, "D", "");
		long blogPostE = createBlogPost(TEST_SPACE_KEY, "E", "{excerpt}E{excerpt-include:F}{excerpt}");
		long pageF = createPage(TEST_SPACE_KEY, "F", "{excerpt}F{excerpt-include:D}{excerpt}");

        updatePage(pageD, "{excerpt}D{excerpt-include:" + getBlogpostWikiValue("E") +"}{excerpt}");

		viewPageById(pageD);
		assertTextPresent("Unable to render excerpt. Already included page E.");
		viewPageById(blogPostE);
		assertTextPresent("Unable to render excerpt. Already included page F.");
		viewPageById(pageF);
		assertTextPresent("Unable to render excerpt. Already included page D.");
	}

	public void testBothIncludeAndExcerptInclude()
	{
		long pageD = createPage(TEST_SPACE_KEY, "D", "{excerpt}D{excerpt-include:E}{excerpt}\n{include:E}");
		createPage(TEST_SPACE_KEY, "E", "{excerpt}A short excerpt{excerpt}\nSome text");

		viewPageById(pageD);
		assertTextPresent("D");
        assertTextPresent("A short excerpt");
		assertTextPresent("Some text");
		assertTextNotPresent("Unable to render {excerpt-include}.");
	}

	private String getBlogpostWikiValue(String title)
	{
		return DATE_FORMAT.format(new Date()) + title;
	}

    public void testIncludeExcerptFromOtherPageTileContainsSpecialCharacters()
     {
         String otherPageTitle = "test ^ & ! ? / title";
         String otherPageContent = "otherPageContent";

         createPage(TEST_SPACE_KEY, otherPageTitle, "{excerpt}" + otherPageContent + "{excerpt}");
         long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPage", "{excerpt-include:" + otherPageTitle + "}");

         viewPageById(testPageId);
         assertEquals(otherPageTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
         assertEquals(otherPageContent, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
     }

     public void testIncludeContentFromOtherPageTileContainsColon()
     {
         String otherPageTitle = "test:title";
         String otherPageContent = "otherPageContent";

         createPage(TEST_SPACE_KEY, otherPageTitle, "{excerpt}" + otherPageContent + "{excerpt}");
         long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPage", "{excerpt-include:" + TEST_SPACE_KEY+ ":" +otherPageTitle + "}");

         viewPageById(testPageId);
         assertEquals(otherPageTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
         assertEquals(otherPageContent, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
     }

     public void testIncludeContentFromOtherPageTileContainsSpecialCharacterAndOtherSpaceKey()
     {
         String otherSpaceKey = "osk";
         String otherPageTitle = "test : ^ & ! ? title";
         String otherPageContent = "otherPageContent";

         createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);

         createPage(otherSpaceKey, otherPageTitle, "{excerpt}" + otherPageContent + "{excerpt}");
         long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromOtherPageInOtherSpace", "{excerpt-include:" + otherSpaceKey + ":" + otherPageTitle + "}");

         viewPageById(testPageId);
         assertEquals(otherPageTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
         assertEquals(otherPageContent, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
     }

     public void testIncludeContentFromBlogPostTitleContainsSpecialCharacters()
     {
         String blogTitle = "blogpost / ^ & ! ? title";
         String blogContent = "This is content from a blog post!";
         Date now = new Date();

         createBlogPost(TEST_SPACE_KEY, blogTitle, "{excerpt}" + blogContent + "{excerpt}");
         long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpost",
                 "{excerpt-include:/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

         viewPageById(testPageId);
         assertEquals(blogTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
         assertEquals(blogContent, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
     }

     public void testIncludeContentFromBlogPostTitleContainsColon()
     {
         String blogTitle = "blogpost:title";
         String blogContent = "This is content from a blog post!";
         Date now = new Date();

         createBlogPost(TEST_SPACE_KEY, blogTitle, "{excerpt}" + blogContent + "{excerpt}");
         long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpost",
                 "{excerpt-include:" + TEST_SPACE_KEY + ":/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

         viewPageById(testPageId);
         assertEquals(blogTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
         assertEquals(blogContent, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
     }

     public void testIncludeContentFromBlogpostContainsSpecialCharactersInOtherSpace()
     {
         String otherSpaceKey = "osk";
         String blogTitle = "blogpost / ^ & ! ? title";
         String blogContent = "This is content from a blog post!";
         Date now = new Date();

         createSpace(otherSpaceKey, otherSpaceKey, otherSpaceKey);
         createBlogPost(otherSpaceKey, blogTitle, "{excerpt}" + blogContent + "{excerpt}");
         long testPageId = createPage(TEST_SPACE_KEY, "testIncludeContentFromBlogpostInOtherSpace",
                 "{excerpt-include:" + otherSpaceKey + ":/" + BlogPost.toDatePath(now) + "/" + blogTitle + "}");

         viewPageById(testPageId);
         assertEquals(blogTitle, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelHeader']"));
         assertEquals(blogContent, getElementTextByXPath("//div[@class='wiki-content']/div[contains(@class,'panel')]/div[@class='panelContent']"));
     }
}
