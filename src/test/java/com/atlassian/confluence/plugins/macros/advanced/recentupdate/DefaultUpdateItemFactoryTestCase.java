package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.bonnie.Handle;
import com.atlassian.confluence.core.DateFormatter;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.search.lucene.DocumentFieldName;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.util.actions.ContentTypesDisplayMapper;
import com.atlassian.confluence.util.i18n.I18NBean;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


public class DefaultUpdateItemFactoryTestCase extends AbstractTestCase
{
    @Mock AnyTypeDao anyTypeDao;
    @Mock DateFormatter dateFormatter;
    @Mock ContentTypesDisplayMapper contentTypesDisplayMapper;
    @Mock I18NBean i18NBean;
    @Mock SearchResult searchResult;

    private DefaultUpdateItemFactory updateItemFactory;

    @Override
    protected Map<String, ?> getAdditionalSingletons()
    {
        Map<String, Object> additionalSingletons = new HashMap<String, Object>();

        additionalSingletons.put("anyTypeDao", anyTypeDao);
        return additionalSingletons;
    }

    public void testPersonalInformationAttachmentConvertedToProfilePictureUpdateItem()
    {
        Attachment personalInfoAttachment = new Attachment();
        personalInfoAttachment.setContent(new PersonalInformation());
        when(searchResult.getType()).thenReturn(Attachment.CONTENT_TYPE);
        when(anyTypeDao.findByHandle(Mockito.<Handle>anyObject())).thenReturn(personalInfoAttachment);

        final UpdateItem updateItem = updateItemFactory.get(searchResult);

        assertEquals(ProfilePictureUpdateItem.class, updateItem.getClass());
    }

    public void testContentAttachmentConvertedToAttachmentUpdateItem()
    {
        Attachment contentAttachment = new Attachment();
        contentAttachment.setContent(new Page());
        when(searchResult.getType()).thenReturn(Attachment.CONTENT_TYPE);
        when(anyTypeDao.findByHandle(Mockito.<Handle>anyObject())).thenReturn(contentAttachment);

        final UpdateItem updateItem = updateItemFactory.get(searchResult);

        assertEquals(AttachmentUpdateItem.class, updateItem.getClass());
    }

    public void testStaleAttachmentSearchResult()
    {
        Attachment contentAttachment = new Attachment();
        contentAttachment.setContent(new Page());
        when(searchResult.getType()).thenReturn(Attachment.CONTENT_TYPE);
        when(anyTypeDao.findByHandle(Mockito.<Handle>anyObject())).thenReturn(null);

        assertNull(updateItemFactory.get(searchResult)); 
    }

    public void testPersonalInformationUpdatesAreConverted()
    {
        when(searchResult.getType()).thenReturn(PersonalInformation.CONTENT_TYPE);
        when(searchResult.getExtraFields()).thenReturn(Collections.singletonMap(DocumentFieldName.CONTENT_VERSION, "2"));

        final UpdateItem updateItem = updateItemFactory.get(searchResult);

        assertEquals(ProfileUpdateItem.class, updateItem.getClass());
    }

    public void testFirstVersionsOfPersonalInformationAreIgnored()
    {
        when(searchResult.getType()).thenReturn(PersonalInformation.CONTENT_TYPE);
        when(searchResult.getExtraFields()).thenReturn(Collections.singletonMap(DocumentFieldName.CONTENT_VERSION, "1"));

        assertNull(updateItemFactory.get(searchResult));
    }

    public void testConvertCommentResults()
    {
        assertResultTypeConvertedToCorrectUpdateItemClass(Comment.CONTENT_TYPE, CommentUpdateItem.class);
    }

    private void assertResultTypeConvertedToCorrectUpdateItemClass(String searchResultType, Class expectedUpdateItemClass)
    {
        when(searchResult.getType()).thenReturn(searchResultType);

        final UpdateItem updateItem = updateItemFactory.get(searchResult);

        assertEquals(expectedUpdateItemClass, updateItem.getClass());
    }

    protected void setUp() throws Exception
    {
        super.setUp();

        updateItemFactory = new DefaultUpdateItemFactory(dateFormatter, i18NBean, contentTypesDisplayMapper);
    }
}
