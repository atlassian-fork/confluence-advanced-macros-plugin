package com.atlassian.confluence.plugins.macros.advanced;

import bucket.core.persistence.hibernate.HibernateHandle;
import com.atlassian.bonnie.AnyTypeObjectDao;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.service.PredefinedSearchBuilder;
import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.user.ConfluenceUserImpl;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.InvalidDurationException;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SearchMacroTestCase extends TestCase
{
    private SearchMacro searchMacro;
	private Map<String, String> params;

    @Mock private UserAccessor userAccessor;
	@Mock private ContentEntityObject contentEntityObject;
	@Mock private SearchResult searchResult;
	@Mock private SearchResults searchResults;
	@Mock private PageContext pageContext;
	@Mock private AnyTypeObjectDao anyTypeObjectDao;
	@Mock private PredefinedSearchBuilder predefinedSearchBuilder;
	@Mock private SearchManager searchManager;
	@Mock private ISearch iSearch;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		
		MockitoAnnotations.initMocks(this);
		
		params = new HashMap<String, String>();

        when(userAccessor.getUserByName("Administrator")).thenReturn(new ConfluenceUserImpl("Administrator", null, null));

        ContainerContext containerContext = mock(ContainerContext.class);
        when(containerContext.getComponent("userAccessor")).thenReturn(userAccessor);
        ContainerManager.getInstance().setContainerContext(containerContext);
    }
	
	public void testSearchByQueryWithoutMaxLimit() throws MacroException, InvalidSearchException
	{
	    final String queryString = "Testing Page";
	    
	    final List<SearchResult> results = new ArrayList<SearchResult>();
	    final Map<String, Object> contextMap = new HashMap<String, Object>();
	    
	    searchMacro = new TestSearchMacro()
	    {
	        @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) 
            {
	            assertEquals(results, contextMap.get("searchResults"));
	            assertEquals(0, contextMap.get("totalSearchResults"));
                assertEquals(10, contextMap.get("maxLimit"));
                assertEquals(queryString, contextMap.get("query"));
                
                return "Finish Executing";
            }
	        
	        @Override
            protected ISearch buildSiteSearch(
                    SearchQueryParameters searchQueryParams, Integer maxLimit) 
            {
                assertEquals("(" +queryString+") AND NOT handle: " +new HibernateHandle(contentEntityObject), searchQueryParams.getQuery());
                assertEquals((Integer)10, maxLimit);
                
                return iSearch;
            }
	        
	        @Override
            protected Map<String, Object> getDefaultVelocityContext() 
            {
                return contextMap;
            }
	    };
	    
	    params.put("query", queryString);
	    
	    when(searchManager.search(iSearch)).thenReturn(searchResults);
        when(searchResults.getAll()).thenReturn(results);
        when(pageContext.getEntity()).thenReturn(contentEntityObject);
	    
	    assertEquals("Finish Executing", searchMacro.execute(params, "", pageContext));
	}
	
	public void testSearchBySpaceKeyWithMaxLimit() throws MacroException, InvalidSearchException
	{
	    final String spaceKey = "tst";
	    
	    final List<SearchResult> results = new ArrayList<SearchResult>();
        final Map<String, Object> contextMap = new HashMap<String, Object>();
        
        searchMacro = new TestSearchMacro()
        {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) 
            {
                assertEquals(results, contextMap.get("searchResults"));
                assertEquals(0, contextMap.get("totalSearchResults"));
                assertEquals(2, contextMap.get("maxLimit"));
                assertEquals(null, contextMap.get("query"));
                
                return "Finish Executing";
            }
            
            @Override
            protected ISearch buildSiteSearch(
                    SearchQueryParameters searchQueryParams, Integer maxLimit) 
            {
                assertTrue(searchQueryParams.getSpaceKeys().contains(spaceKey));
                assertEquals((Integer)2, maxLimit);
                
                return iSearch;
            }
            
            @Override
            protected Map<String, Object> getDefaultVelocityContext() 
            {
                return contextMap;
            }
        };
        
        params.put("spacekey", spaceKey);
	    params.put("maxLimit", "2");
        
        when(searchManager.search(iSearch)).thenReturn(searchResults);
        when(searchResults.getAll()).thenReturn(results);
        
	    assertEquals("Finish Executing", searchMacro.execute(params, "", pageContext));
	}

	public void testSearchByTypeWithoutMaxLimit() throws MacroException, InvalidSearchException
	{
	    final List<SearchResult> results = new ArrayList<SearchResult>();
        final Map<String, Object> contextMap = new HashMap<String, Object>();
        
        searchMacro = new TestSearchMacro()
        {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) 
            {
                assertEquals(results, contextMap.get("searchResults"));
                assertEquals(1, contextMap.get("totalSearchResults"));
                assertEquals(10, contextMap.get("maxLimit"));
                assertEquals(null, contextMap.get("query"));
                
                return "Finish Executing";
            }
            
            @Override
            protected ISearch buildSiteSearch(
                    SearchQueryParameters searchQueryParams, Integer maxLimit) 
            {
                assertEquals(new HashSet<ContentTypeEnum>(Arrays.asList(ContentTypeEnum.PAGE)), searchQueryParams.getContentTypes());
                assertEquals((Integer)10, maxLimit);
                
                return iSearch;
            }

            @Override
            protected Map<String, Object> getDefaultVelocityContext() 
            {
                return contextMap;
            }
        };
        
        results.add(searchResult);
        
        params.put("type", ContentTypeEnum.PAGE.toString());
        
	    when(searchManager.search(iSearch)).thenReturn(searchResults);
        when(searchResults.getAll()).thenReturn(results);
	    
	    assertEquals("Finish Executing", searchMacro.execute(params, "", pageContext));
	}
	
	public void testSearchByLastModifiedWithMaxLimit() throws MacroException, InvalidDurationException, InvalidSearchException
	{
	    final String lastModified = "600";
	    
	    final List<SearchResult> results = new ArrayList<SearchResult>();
	    final Map<String, Object> contextMap = new HashMap<String, Object>();
	    
	    final long expectedDuration = DateUtils.getDuration(lastModified);
        final Date currentDateLessDuration = new Date(System.currentTimeMillis() - (expectedDuration * 1000));
        final DateRangeQuery.DateRange dateRange = new DateRangeQuery.DateRange(currentDateLessDuration, null, true, false);
	    
	    searchMacro = new TestSearchMacro()
	    {
	        @Override
            protected Date getCurrentDateLessDuration(long duration) 
	        {
	            assertEquals(expectedDuration, duration);
                return currentDateLessDuration;
            }

            @Override
	        protected String getRenderedTemplate(Map<String, Object> contextMap) 
	        {
	            assertEquals(results, contextMap.get("searchResults"));
	            assertEquals(0, contextMap.get("totalSearchResults"));
	            assertEquals(3, contextMap.get("maxLimit"));
	            
	            return "Finish Executing";
	        }
	        
	        @Override
	        protected ISearch buildSiteSearch(
	                SearchQueryParameters searchQueryParams, Integer maxLimit) 
	        {
	            assertEquals(dateRange.getFrom(), searchQueryParams.getLastModified().getFrom());
	            assertEquals((Integer)3, maxLimit);
	            
	            return iSearch;
	        }

	        @Override
	        protected Map<String, Object> getDefaultVelocityContext() 
	        {
	            return contextMap;
	        }
	    };
	    
	    params.put("lastModified", lastModified);
	    params.put("maxLimit", "3");
	    
	    when(searchManager.search(iSearch)).thenReturn(searchResults);
	    when(searchResults.getAll()).thenReturn(results);
	    
	    assertEquals("Finish Executing", searchMacro.execute(params, "", pageContext));
	}
	
	public void testSearchByContributorWithoutMaxLimit() throws MacroException, InvalidSearchException
	{
	    final String contributor = "Administrator";
	    
	    final List<SearchResult> results = new ArrayList<SearchResult>();
        final Map<String, Object> contextMap = new HashMap<String, Object>();
	    
	    searchMacro = new TestSearchMacro()
        {
            @Override
            protected String getRenderedTemplate(Map<String, Object> contextMap) 
            {
                assertEquals(results, contextMap.get("searchResults"));
                assertEquals(0, contextMap.get("totalSearchResults"));
                assertEquals(10, contextMap.get("maxLimit"));
                
                return "Finish Executing";
            }
            
            @Override
            protected ISearch buildSiteSearch(
                    SearchQueryParameters searchQueryParams, Integer maxLimit) 
            {
                assertEquals(contributor, searchQueryParams.getContributor().getName());
                assertEquals((Integer)10, maxLimit);
                
                return iSearch;
            }

            @Override
            protected Map<String, Object> getDefaultVelocityContext() 
            {
                return contextMap;
            }
        };
	    
	    params.put("contributor", contributor);
	    
	    when(searchManager.search(iSearch)).thenReturn(searchResults);
        when(searchResults.getAll()).thenReturn(results);
	    
	    assertEquals("Finish Executing", searchMacro.execute(params, "", pageContext));
	}
	
	public void testIsInline()
	{
	    searchMacro = new TestSearchMacro();
        
        assertEquals(false, searchMacro.isInline());
	}
	
	public void testHasBody()
	{
	    searchMacro = new TestSearchMacro();
        
        assertEquals(false, searchMacro.hasBody());
	}
	
	public void testRenderMode()
	{
	    searchMacro = new TestSearchMacro();
	    
	    assertEquals(RenderMode.NO_RENDER, searchMacro.getBodyRenderMode());
	}
	
	private class TestSearchMacro extends SearchMacro
	{
	    public TestSearchMacro()
	    {
	        setPredefinedSearchBuilder(predefinedSearchBuilder);
	        setSearchManager(searchManager);
	    }
	}
	
}
