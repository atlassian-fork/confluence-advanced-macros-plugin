package com.atlassian.confluence.plugins.macros.advanced.recentupdate;

import com.atlassian.confluence.plugins.macros.advanced.AbstractTestCase;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.actions.ProfilePictureInfo;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.spring.container.ContainerContext;
import org.mockito.Mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

public class UserProfileLinkTestCase extends AbstractTestCase
{
    private UserProfileLink userProfileLink;

    private String resourcePrefix = "template";

    @Mock private ContainerContext containerContext;
    @Mock private WebResourceUrlProvider webResourceUrlProvider;
    @Mock private UserAccessor userAccessor;
    @Mock private ConfluenceUser user;
    @Mock private ProfilePictureInfo profilePicture;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        when(webResourceUrlProvider.getStaticResourcePrefix(UrlMode.AUTO)).thenReturn(resourcePrefix);
    }

    @Override
    protected Map<String, ?> getAdditionalSingletons()
    {
        Map<String, Object> singletons = new HashMap<String, Object>();
        singletons.put("webResourceUrlProvider", webResourceUrlProvider);
        singletons.put("userAccessor", userAccessor);

        return singletons;
    }

    public void testGetLinkBodyWithAnonymousUser()
    {
        String title = "Anonymous";

        userProfileLink = new UserProfileLink(null, i18n);

        when(i18n.getText("user.icon.anonymous.title")).thenReturn(title);

        assertEquals("<img class=\"userLogo logo anonymous\" src=\"" + resourcePrefix + "/images/icons/profilepics/anonymous.png\" " +
        		"alt=\"\" title=\"" + title + "\">",
                userProfileLink.getLinkBody());
    }

    public void testGetLinkBodyWithLoginUser()
    {
        String username = "Administrator";
        String downloadPath = "/images/icon/angel.png";

        when(userAccessor.getUser(username)).thenReturn(user);
        when(userAccessor.getUserProfilePicture(user)).thenReturn(profilePicture);

        userProfileLink = new UserProfileLink(username, i18n);

        when(profilePicture.isDefault()).thenReturn(true);
        when(profilePicture.getDownloadPath()).thenReturn(downloadPath);

        assertEquals("<img class=\"userLogo logo\" src=\"" + resourcePrefix + downloadPath + "\" alt=\"\" title=\"" + username + "\">",
                userProfileLink.getLinkBody());
    }
}
